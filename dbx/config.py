"""
config newton
"""

import json
import os
import typing

from dataclasses import dataclass


@dataclass
class Config:
    key: str | None
    refresh_token: str | None

    def save(self, fp: typing.IO):
        """
        write json to fp
        """
        json.dump(self.to_dict(), fp)

    def to_dict(self) -> dict[str, str | None]:
        return {
            "DBX_KEY": self.key,
            "DBX_REFRESH_TOKEN": self.refresh_token,
        }

    @classmethod
    def empty(cls):
        return cls(None, None)

    @classmethod
    def from_json_file(cls, fp: typing.IO):
        config = json.load(fp)

        return cls(
            env_or_json(config, "DBX_KEY"), env_or_json(config, "DBX_REFRESH_TOKEN")
        )


def env_or_json(config: dict, key: str) -> str | None:
    return os.environ.get(key, config.get(key))
