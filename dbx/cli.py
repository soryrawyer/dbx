"""
click click click
"""
import functools
import itertools
import os
import typing

from collections.abc import Callable, Generator
from contextlib import contextmanager
from dataclasses import dataclass
from pathlib import Path

import click

import dropbox
import dropbox.exceptions
from dropbox import DropboxOAuth2FlowNoRedirect
from dropbox.files import FileMetadata, FolderMetadata

from tabulate import tabulate

from dbx.ascii_art import ASCII_SHADOW
from dbx.config import Config
from dbx import storage


APP_NAME = "dbx"
CONFIG_PATH = Path(
    os.environ.get(
        "DBX_CONFIG_PATH", os.path.join(click.get_app_dir(APP_NAME), "config.json")
    )
)
CONTEXT_SETTINGS = dict(help_option_names=["-h", "--help"])


def get_logo(logo: str = ASCII_SHADOW) -> str:
    colors = itertools.cycle(["green", "red", "yellow", "blue", "magenta", "cyan"])
    lines = []
    for line, color in zip(logo.split("\n"), colors):
        lines.append(click.style(line, fg=color))
    return "\n".join(lines)


def error(message: str):
    click.secho(message, fg="red")
    raise click.Abort()


class DbxGroup(click.Group):
    def format_help(self, ctx: click.Context, formatter: click.HelpFormatter) -> None:
        click.echo(get_logo())
        return super().format_help(ctx, formatter)


@contextmanager
def dbx_connection(config: Config) -> Generator[dropbox.Dropbox, None, None]:
    key = config.key
    refresh_token = config.refresh_token
    if key is None or refresh_token is None:
        error("Dropbox credentials not found. Please run 'dbx oauth' to authenticate")
    with dropbox.Dropbox(
        oauth2_refresh_token=refresh_token,
        app_key=key,
    ) as dbx:
        yield dbx


@dataclass
class CliContext:
    config: Config
    # TODO: figure out what's up with the generator/context manager type hint situation
    dbx_provider: Callable[
        [str, str], Generator[dropbox.Dropbox, None, None]
    ] = dbx_connection  # type: ignore


@dataclass
class PathArg:
    """
    dataclass for holding onto some info about input paths
    """

    original_path: Path
    path_type: storage.PathType
    file_input: storage.FileInput


@click.group(cls=DbxGroup, context_settings=CONTEXT_SETTINGS)
@click.pass_context
def cli(ctx: click.Context, config: CliContext | None = None):
    ctx.ensure_object(CliContext)
    if config is not None:
        ctx.obj = config


@cli.command()
@click.pass_context
@click.argument("source-paths", type=click.Path(path_type=Path), nargs=-1)
@click.argument("destination-path", type=click.Path(path_type=Path))
@click.option("--dry-run", is_flag=True)
def cp(
    ctx: click.Context,
    source_paths: list[Path],
    destination_path: Path,
    dry_run: bool,
):
    """
    this command will copy a source file to dropbox

    source: path to file or directory to copy

    destination: full path of upload destination in dropbox.
    if copying a single file, destination can either be a directory or a full file path.
    if copying multiple files, the destination location must be an existing directory
    """
    with ctx.obj.dbx_provider(ctx.obj.config) as dbx:
        (sources, destination) = validate_copy_input_paths(
            dbx, source_paths, destination_path
        )
        for source in sources:
            click.echo(
                (
                    f"writing {source.path_type.value} file {source.original_path}"
                    f" to {destination.path_type.value} at {destination.original_path}"
                )
            )
            if dry_run:
                continue

            # save the function we call in case we want to rerun this operation
            # based on operator feedback later on
            op = functools.partial(do_copy, dbx, source, destination)
            try:
                op()
            except storage.CollisionException:
                click.echo("1. do nothing")
                click.echo("2. overwrite the existing file")
                value = click.prompt("pick onE!!!", type=int)
                if value == 1:
                    click.echo(f"skipping {source.original_path}")
                elif value == 2:
                    # rerun our original operation, but specifying that we should
                    # overwrite the existing file
                    op(overwrite=True)
                else:
                    error("please enter a valid choice")

    click.echo("all done!")
    return


def validate_copy_input_paths(
    dbx: dropbox.Dropbox, source_paths: list[Path], destination_path: Path
) -> typing.Tuple[list[PathArg], PathArg]:
    """
    valid combinations:
    list of local files, dropbox folder
    list of dropbox files, local folder
    one local file, dropbox folder
    one local file, dropbox file
    one dropbox file, local folder
    one dropbox file, local file
    """
    sources = list(enrich_path(dbx, i) for i in source_paths)
    if len(set(i.path_type for i in sources)) > 1:
        error("cannot mix local and remote paths in source argument")

    destination = enrich_path(dbx, destination_path)

    source_dest_local = (
        sources[0].path_type == destination.path_type == storage.PathType.Local
    )
    if source_dest_local and destination.original_path != Path("/"):
        error("source and destination cannot both be local")
    return (sources, destination)


def enrich_path(dbx: dropbox.Dropbox, path: Path) -> PathArg:
    (path_type, meta) = storage.explain_input_path(dbx, path)
    return PathArg(path, path_type, meta)


def do_copy(
    dbx: dropbox.Dropbox,
    source: PathArg,
    destination: PathArg,
    overwrite: bool = False,
):
    # valid local/remote combos:
    # - local directory/remote directory
    # - remote directory/local directory
    # - local file/remote file
    # - local file/remote directory
    # TODO: support remote/remote
    match (source.file_input, destination.file_input):
        case (None, _):
            error("source location must be valid local or dropbox location")

        case (Path() as p, FolderMetadata() as dest):
            if os.path.isdir(p):
                error("please use shell path expansion to copy an entire directory")

            storage.copy_from_filename(
                dbx, p, Path(dest.path_display), overwrite=overwrite
            )

        case (Path() as source_path, Path() as dest):
            # this is a special case to handle copying to the root
            # dropbox directory
            if str(dest) != "/":
                error("if source location is local, destination must be remote")
            storage.copy_from_filename(dbx, source_path, dest, overwrite=overwrite)

        case (FileMetadata(path_display=source_path_str), Path() as dest):
            if dest.is_dir():
                dest = Path(os.path.join(dest, os.path.basename(source_path_str)))
            storage.download_to_filename(dbx, dest, Path(source_path_str))

        case (FileMetadata(path_display=remote), None):
            storage.download_to_filename(dbx, destination.original_path, Path(remote))

        case _ as hm:
            error(f"uh oh! what's this? {hm}")


@cli.command()
@click.pass_context
@click.argument("path", type=click.Path(path_type=Path))
def ls(ctx: click.Context, path: Path):
    """
    show information for a given path

    path: full path to dropfox folder or file you'd like to list.
    if path is a file, basic file information is displayed.
    if path is a folder, the folder contents are listed.
    """
    if str(path) == ".":
        error("PATH must be a valid dropbox path")
    click.secho(f"listing information for {str(path)}:", fg="green")
    with ctx.obj.dbx_provider(ctx.obj.config) as dbx:
        try:
            results = list(storage.list_folder(dbx, str(path)))
            click.secho(
                tabulate(storage.rearrange_for_tabulate(results), headers="keys"),
                fg="cyan",
            )
            click.secho(f"total: {len(results)}", bold=True)
        except dropbox.exceptions.DropboxException as err:
            error(f"dropbox would like to have a word: {err}")
        except Exception as err:
            click.echo(f"got some rando error when listing files: {type(err)}")
            error(f"message: {err}")
    return


@cli.command()
@click.pass_context
@click.argument("path", type=click.Path(path_type=Path))
@click.option("--dry-run", is_flag=True)
def rm(ctx: click.Context, path: Path, dry_run: bool):
    """
    remove the given path

    path: full path of dropbox location to delete. if it is a directory, dbx will
    ask for confirmation that all files should be deleted
    """
    if str(path) == "/":
        error("cannot remove root path")
    if str(path) == ".":
        error("PATH must be a valid Dropbox location")

    with ctx.obj.dbx_provider(ctx.obj.config) as dbx:
        metadata = storage.get_db_path_metadata(dbx, path)
        click.echo(f"removing {path}")
        if dry_run:
            return

        if metadata is None:
            error(f"{path} was not found")
        elif isinstance(metadata, FolderMetadata):
            if not click.confirm(
                f"are you sure you want to delete the entire folder '{metadata.name}'?"
            ):
                click.echo(f"leaving {path} as it is")
                return
        try:
            dbx.files_delete(str(path))
            click.echo(f"successfully deleted {path}.")
        except dropbox.exceptions.ApiError as err:
            click.echo(f"oh no! {err}")


@cli.command()
@click.pass_context
@click.argument("path", type=click.Path(path_type=Path))
def mkdir(ctx: click.Context, path: Path):
    """
    make a directory
    """

    with ctx.obj.dbx_provider(ctx.obj.config) as dbx:
        # let's see if anything's already here
        metadata = storage.get_db_path_metadata(dbx, path)
        if metadata is not None:
            # TODO: reevaluate if bailing is the right move here
            # it looks like the dropbox web ui default is to create the
            # directory with a new name (e.g. name (1); name (2); etc.)
            # we could use the `autorename` kwarg in files_create_folder
            entry_type = "file" if isinstance(metadata, FileMetadata) else "folder"
            error(
                f"{path} already exists as a {entry_type} and dbx will not overwrite it"
            )

        if os.path.dirname(path) != "/":
            # if we're not operating at the root directory, make sure the
            # parent folder isn't actually a file
            parent_meta = storage.get_db_path_metadata(dbx, Path(os.path.dirname(path)))
            if isinstance(parent_meta, FileMetadata):
                error(f"parent {parent_meta.path_display} already exists as a file")

        dbx.files_create_folder(str(path))
        click.echo(f"successfully created {path}.")


@cli.command()
@click.pass_context
def oauth(ctx: click.Context):
    """
    generate an oauth token for this application
    """
    dbx_key = click.prompt("enter api key").strip()
    auth_flow = DropboxOAuth2FlowNoRedirect(
        dbx_key,
        use_pkce=True,
        token_access_type="offline",
    )

    auth_url = auth_flow.start()
    click.echo(f"go to {auth_url}")
    click.echo("click 'allow'")
    click.echo("copy auth code")
    code = click.prompt("enter auth code").strip()

    try:
        result = auth_flow.finish(code)
    except Exception as err:
        click.echo(f"uh oh! {err}")
        raise

    click.echo("testing new token")
    config = Config(dbx_key, result.refresh_token)
    with ctx.obj.dbx_provider(config) as dbx:
        dbx.users_get_current_account()
        click.echo("it works!")

    # we're re-checking the config_path because the unit tests set the environment
    # variable _after_ the module is loaded, so this module doesn't know there's a
    # non-default config path until after it checks for such a thing
    config_path = os.environ.get(
        "DBX_CONFIG_PATH", os.path.join(click.get_app_dir(APP_NAME), "config.json")
    )
    if not os.path.isdir(os.path.dirname(config_path)):
        os.makedirs(os.path.dirname(config_path))
    with open(config_path, "w") as config_fp:
        config.save(config_fp)

    click.echo("dbx config saved")
