"""
store and load files
"""

import enum
import os

from collections.abc import Callable
from dataclasses import dataclass
from pathlib import Path
from typing import (
    Generator,
    BinaryIO,
    Literal,
    Tuple,
    assert_never,
)

import dropbox
import dropbox.exceptions
from dropbox.files import (
    CommitInfo,
    FileMetadata,
    FolderMetadata,
    GetMetadataError,
    ListFolderResult,
    UploadError,
    UploadSessionCursor,
    UploadSessionStartResult,
    WriteMode,
)

import tqdm


FILE_TRANSFER_CHUNK_SIZE = 150 * 1024 * 1024  # upload, at most, 150Mb at once


class CollisionException(Exception):
    pass


class MetadataException(Exception):
    pass


@dataclass
class Entry:
    name: str
    directory: str
    entry_type: Literal["file"] | Literal["directory"]
    size: int | None = None


class PathType(enum.Enum):
    Dropbox = "dropbox"
    Local = "local"
    Nonexistent = "nonexistent"


FileInput = Path | FileMetadata | FolderMetadata | None


def get_absolute_path(p: Path) -> Path:
    """best-effort attempt to expand relative paths"""

    # note: the application order of expanduser and abspath is intentional
    return Path(os.path.abspath(os.path.expanduser(p)))


def explain_input_path(
    dbx: dropbox.Dropbox, source: Path
) -> Tuple[PathType, FileInput]:
    """
    return information about the source and destination paths

    the rules are:
    1. if a path exists locally and isn't the root directory, return the Path object
    2. if the path doesn't start with a forward slash, return None
       returning None, as opposed to raising an exception, feels like a better
       way to let calling functions deal with the case where an input Path doesn't exist
    3. look up the path in dropbox and return whatever we find there
    """
    if source.exists():
        if source.is_absolute():
            return (PathType.Local, source)
        else:
            # life will be easier if we expand relative directories here
            return (PathType.Local, get_absolute_path(source))
    elif source.is_absolute():
        return (PathType.Dropbox, get_db_path_metadata(dbx, source))
    else:
        # dropbox paths must be absolute, so this must be a local path
        # that just doesn't exist yet
        return (PathType.Local, get_absolute_path(source))


def list_folder(dbx: dropbox.Dropbox, folder: str) -> Generator[Entry, None, None]:
    if folder == "/":
        folder = ""

    result: ListFolderResult = dbx.files_list_folder(folder)
    while True:
        for entry in result.entries:
            if isinstance(entry, FileMetadata) or isinstance(entry, FolderMetadata):
                yield to_dir_entry(entry, folder)

        if not result.has_more:
            break
        result = dbx.files_list_folder_continue(result.cursor)
    return


def to_dir_entry(item: FolderMetadata | FileMetadata, folder: str) -> Entry:
    match item:
        case FileMetadata(name=name, size=size):
            return Entry(name, folder, "file", size)
        case FolderMetadata(name=name):
            return Entry(name, folder if folder != "" else "/", "directory")
        case _ as unreachable:
            assert_never(unreachable)


def copy_file(
    dbx: dropbox.Dropbox,
    local: BinaryIO,
    remote: Path,
    mode: WriteMode = WriteMode.add,
):
    """
    write the contents of local file to remote directory

    from the dropbox docs:
    - a single request should not upload more than 150mb
    - a single upload session cannot exceed 350gb

    if the dropbox client raises an ApiError, this function will:
    - raise a CollisionException if the error was a write conflict
    - raise the original error otherwise
    """
    collision = detect_collision(dbx, os.path.basename(local.name), remote)
    if mode == WriteMode.add and collision:
        raise CollisionException()

    full_remote = os.path.join(remote if remote else "/", os.path.basename(local.name))
    try:
        file_size = os.path.getsize(local.name)
        if os.path.getsize(local.name) <= FILE_TRANSFER_CHUNK_SIZE:
            dbx.files_upload(local.read(), full_remote, mode=mode)
        else:
            # TODO: use tqdm to show a sick progress bar
            resp: UploadSessionStartResult = dbx.files_upload_session_start(
                local.read(FILE_TRANSFER_CHUNK_SIZE)
            )
            cursor = UploadSessionCursor(resp.session_id, offset=local.tell())
            commit = CommitInfo(full_remote)
            offsets = range(cursor.offset, file_size, FILE_TRANSFER_CHUNK_SIZE)
            for _ in offsets:
                data = local.read(FILE_TRANSFER_CHUNK_SIZE)
                dbx.files_upload_session_append_v2(data, cursor)
                cursor.offset += len(data)
            dbx.files_upload_session_finish(b"", cursor, commit)
    except dropbox.exceptions.ApiError as err:
        # raise a collision exception if the upload failed because of
        # a write conflict
        if (
            isinstance(err.error, UploadError)
            and err.error.is_path()
            and err.error.get_path().reason.is_conflict()
        ):
            raise CollisionException()

        raise err


def copy_from_filename(
    dbx: dropbox.Dropbox, local: Path, remote: Path, overwrite: bool = False
):
    if overwrite:
        mode = WriteMode.update
    else:
        mode = WriteMode.add
    return operate_on_filename(dbx, local, remote, copy_file, mode=mode)


def download_to_filename(dbx: dropbox.Dropbox, local: Path, remote: Path):
    return operate_on_filename(dbx, local, remote, download_file)


def operate_on_filename(
    dbx: dropbox.Dropbox,
    local: Path,
    remote: Path,
    func: Callable[[dropbox.Dropbox, BinaryIO, Path], None],
    **kwargs,
):
    # if the file doesn't exist locally, create it when opening
    mode = "rb" if local.exists() else "wb"
    with open(local, mode=mode) as local_file:
        func(dbx, local_file, remote, **kwargs)


def download_file(dbx: dropbox.Dropbox, local: BinaryIO, remote: Path):
    (meta, resp) = dbx.files_download(os.path.expanduser(remote))
    chunk_size = 2048
    num_chunks = int((meta.size / chunk_size) + 1)
    # TODO: customize progress units to show bytes instead of iterations
    for chunk in tqdm.tqdm(resp.iter_content(chunk_size=chunk_size), total=num_chunks):
        local.write(chunk)


def get_local_path_metadata(path: str) -> Path | None:
    if os.path.isfile(path) or os.path.isdir(path):
        return Path(path)
    else:
        return None


def get_db_path_metadata(
    dbx: dropbox.Dropbox, path: Path
) -> FolderMetadata | FileMetadata | None:
    """
    for whenever you want to know: is this a file, folder, or nothing?
    """
    if str(path) == "/" or str(path) == ".":
        raise MetadataException()

    try:
        resp = dbx.files_get_metadata(str(path))
        if isinstance(resp, FolderMetadata) or isinstance(resp, FileMetadata):
            return resp
        else:
            raise MetadataException(
                (
                    f"{path} is neither a folder nor a file, but rather a different,"
                    f" third thing: {type(resp)}"
                )
            )
    except dropbox.exceptions.ApiError as err:
        if not isinstance(err.error, GetMetadataError):
            raise err

        if err.error.is_path() and err.error.get_path().is_not_found():
            return None


def detect_collision(dbx: dropbox.Dropbox, name: str, remote: Path) -> bool:
    # if listing the root directory, dropbox requires an empty string

    dropbox_path = "" if str(remote) == "/" else str(remote)
    try:
        resp: ListFolderResult = dbx.files_list_folder(dropbox_path)
    except dropbox.exceptions.ApiError:
        # no folder with this name
        # try looking for os.path.dirname
        try:
            resp: ListFolderResult = dbx.files_list_folder(os.path.dirname(remote))
        except dropbox.exceptions.ApiError as err:
            # TODO: do something, or at least print a more helpful error message
            # maybe we should just return a custom exception and have the cli
            # component handle how those exceptions are displayed to the operator?
            raise err

    # return yet another new exception type so that the cli code can
    # prompt the user if they want
    # to overwrite or skip or whatever. let the operator of this
    # script determine which WriteMode is used
    matches = list(i for i in resp.entries if i.name == name)
    return bool(matches)


def rearrange_for_tabulate(
    data: list[Entry],
) -> dict[str, list]:
    result = {
        "name": [],
        "directory": [],
        "entry_type": [],
        "size": [],
    }
    for item in sorted(data, key=lambda e: f"{e.entry_type}-{e.name.lower()}"):
        result["name"].append(item.name)
        result["directory"].append(item.directory)
        result["entry_type"].append(item.entry_type)
        result["size"].append(narrow_size(item.size) if item.size else "-")
    return result


def narrow_size(size: int) -> str:
    """
    given a file size, return a reasonable estimation of unit and return
    both the new converted size and the unit of measurement (b, Kb, Mb, etc.)
    """

    def float_to_str(num: float) -> str:
        s = f"{num:.2f}"
        return s.rstrip("0").rstrip(".")

    result = float(size)
    units = ["", "K", "M", "G"]
    for unit in units:
        if result < 1024:
            return f"{float_to_str(result)}{unit}"
        else:
            result /= 1024
    return f"{float_to_str(result)}{units[-1]}"
