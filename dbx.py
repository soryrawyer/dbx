"""
entrypoint for pyinstaller
"""

import os
from dbx.cli import cli, CliContext, CONFIG_PATH
from dbx.config import Config

if os.path.isfile(CONFIG_PATH):
    with open(CONFIG_PATH) as config_fp:
        config = Config.from_json_file(config_fp)
else:
    config = Config(os.environ.get("DBX_KEY"), os.environ.get("DBX_REFRESH_TOKEN"))
context = CliContext(config)
cli(obj=context)
