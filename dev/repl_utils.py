"""
a few things that might be useful during a repl session
"""

import os

if os.environ.get("DBX_REFRESH_TOKEN") is None:
    import dotenv

    dotenv.load_dotenv()

import dropbox

dbx = dropbox.Dropbox(
    oauth2_refresh_token=os.environ["DBX_REFRESH_TOKEN"],
    app_key=os.environ["DBX_KEY"],
)
