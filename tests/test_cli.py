"""
check click for cracks
"""

import json
import os
import re

from contextlib import contextmanager
from dataclasses import dataclass
from unittest.mock import patch

import pytest
from click.testing import CliRunner

from dbx.cli import cli, CliContext
from dbx.config import Config
from tests.common import mock_dbx  # pyright: ignore


@dataclass
class CliTestCase:
    """
    just trying something out here
    """

    command: list[str]
    exit_code: int
    output: str
    invoke_kwargs: dict | None = None
    local_file_exists: bool = False


@pytest.fixture
def dbx_generator(mock_dbx):
    @contextmanager
    def gen(*args, **kwargs):
        del args, kwargs
        yield mock_dbx

    return gen


@pytest.fixture
def cli_context(dbx_generator):
    return CliContext(Config("", ""), dbx_generator)


@pytest.mark.parametrize(
    "test_case",
    [
        CliTestCase(["ls", ""], 1, "PATH must be a valid dropbox path"),
        CliTestCase(["ls", "/"], 0, r"test\s*/\s*directory"),
        CliTestCase(
            ["ls", "/reading/tech"],
            0,
            r"how-containers-work.pdf\s*/reading/tech\s*file",
        ),
    ],
)
def test_ls(cli_context: CliContext, test_case: CliTestCase):
    runner = CliRunner()
    result = runner.invoke(cli, test_case.command, obj=cli_context)
    assert test_case.exit_code == result.exit_code
    assert re.search(test_case.output, result.output)


def test_cp_remote_to_local(cli_context):
    runner = CliRunner()
    with runner.isolated_filesystem():
        # copying from dropbox to local
        db_filename = "/reading/tech/how-containers-work.pdf"
        result = runner.invoke(cli, ["cp", db_filename, ""], obj=cli_context)
        assert result.exit_code == 0
        assert os.path.isfile(os.path.basename(db_filename))
        assert f"writing dropbox file {db_filename} to local" in result.output

        filename = "hcw.pdf"
        result = runner.invoke(
            cli,
            ["cp", db_filename, filename],
            obj=cli_context,
        )
        assert result.exit_code == 0
        assert os.path.isfile(filename)

        filename = "dry-run.pdf"
        result = runner.invoke(
            cli, ["cp", db_filename, filename, "--dry-run"], obj=cli_context
        )
        print(result.output)
        assert result.exit_code == 0
        assert not os.path.isfile(filename)
        assert f"writing dropbox file {db_filename} to local" in result.output


@pytest.mark.parametrize(
    "test_case",
    [
        CliTestCase(["cp", "a.txt", "/"], 0, "all done!", local_file_exists=True),
        CliTestCase(
            ["cp", "file.txt", "/test"],
            0,
            "skipping",
            local_file_exists=True,
            invoke_kwargs={"input": "1"},
        ),
        CliTestCase(
            ["cp", "file.txt", "/test"],
            1,
            "please enter a valid choice",
            local_file_exists=True,
            invoke_kwargs={"input": "4"},
        ),
    ],
)
def test_cp_local_to_remote(cli_context: CliContext, test_case: CliTestCase):
    runner = CliRunner()
    with runner.isolated_filesystem():
        # copying from local to dropbox
        if test_case.local_file_exists:
            with open(test_case.command[1], "w") as f:
                f.write("hi")
        if test_case.invoke_kwargs is not None:
            result = runner.invoke(
                cli, test_case.command, obj=cli_context, **test_case.invoke_kwargs
            )
        else:
            result = runner.invoke(cli, test_case.command, obj=cli_context)
        assert test_case.exit_code == result.exit_code
        assert re.search(test_case.output, result.output)


@pytest.mark.parametrize(
    "test_case",
    [
        CliTestCase(["rm", ""], 1, "PATH must be a valid Dropbox location"),
        CliTestCase(["rm", "/"], 1, "cannot remove root path"),
        CliTestCase(["rm", "/test/file.txt"], 0, "deleted /test/file.txt."),
        CliTestCase(["rm", "/test/file2.txt"], 1, "/test/file2.txt was not found"),
        CliTestCase(["rm", "/test"], 0, "leaving", {"input": "N"}),
        CliTestCase(["rm", "/test"], 0, "deleted", {"input": "y"}),
        CliTestCase(["rm", "/test/"], 0, "deleted", {"input": "y"}),
    ],
)
def test_rm(cli_context: CliContext, test_case: CliTestCase):
    runner = CliRunner()
    if test_case.invoke_kwargs is not None:
        result = runner.invoke(
            cli, test_case.command, obj=cli_context, **test_case.invoke_kwargs
        )
    else:
        result = runner.invoke(cli, test_case.command, obj=cli_context)

    assert test_case.exit_code == result.exit_code
    assert re.search(test_case.output, result.output)


def test_rm_dry_run(cli_context: CliContext):
    runner = CliRunner()
    result = runner.invoke(cli, ["rm", "/test/file.txt", "--dry-run"], obj=cli_context)
    assert result.exit_code == 0
    assert "successfully deleted" not in result.output


@pytest.mark.parametrize(
    "test_case",
    [
        CliTestCase(["mkdir", "/test/file.txt"], 1, "already exists as a file"),
        CliTestCase(["mkdir", "/test"], 1, "already exists as a folder"),
        CliTestCase(["mkdir", "/new_dir"], 0, "successfully created /new_dir"),
        CliTestCase(["mkdir", "/new_dir/"], 0, "successfully created /new_dir."),
        CliTestCase(
            ["mkdir", "/test/new_dir"], 0, "successfully created /test/new_dir"
        ),
        CliTestCase(
            ["mkdir", "/test/file.txt/new_dir"],
            1,
            "parent /test/file.txt already exists as a file",
        ),
        CliTestCase(["mkdir", "/does_not_exist/new_dir"], 0, "successfully created"),
    ],
)
def test_mkdir(cli_context, test_case: CliTestCase):
    runner = CliRunner()
    result = runner.invoke(cli, test_case.command, obj=cli_context)
    assert test_case.exit_code == result.exit_code
    assert re.search(test_case.output, result.output)


def test_help():
    # since -h does not print help text by default, check that -h and --help
    # return the same output
    runner = CliRunner()
    short = runner.invoke(cli, ["-h"])
    full = runner.invoke(cli, ["--help"])
    assert short.output == full.output


class MockAuthFlowResult:
    refresh_token = "hi :)"

    def __init__(self, *args, **kwargs):
        pass


@patch("dropbox.DropboxOAuth2FlowNoRedirect.start", spec=True)
@patch("dropbox.DropboxOAuth2FlowNoRedirect.finish", MockAuthFlowResult, spec=True)
def test_oauth(mock_start, cli_context: CliContext):
    """
    use isolated filesystem
    set config path to a path in said filesystem
    confirm:
    - config file exists at expected location
    - config file contains necessary keys
    """

    runner = CliRunner()
    with runner.isolated_filesystem() as tmp:
        # TODO: look into managing test files with pytest
        config_file_path = os.path.join(tmp, "config.json")
        result = runner.invoke(
            cli,
            ["oauth"],
            obj=cli_context,
            env={"DBX_CONFIG_PATH": config_file_path},
            input="api_key\nauth_code\n",
        )

        assert result.exception is None
        assert "saved" in result.output
        assert os.path.isfile(config_file_path)

        config = json.load(open(config_file_path))
        assert config["DBX_KEY"] == "api_key"
        assert config["DBX_REFRESH_TOKEN"] == "hi :)"
