import io
import itertools
import functools
import os
from pathlib import Path

import pytest
from click.testing import CliRunner

import dropbox.exceptions
import dropbox.files as df
from dbx.storage import (
    CollisionException,
    Entry,
    MetadataException,
    PathType,
    copy_file,
    detect_collision,
    explain_input_path,
    get_db_path_metadata,
    list_folder,
    rearrange_for_tabulate,
    to_dir_entry,
)

from tests import common

mock_dbx = common.mock_dbx

ROOT = Path("/")
CURRENT = Path("")


ENTRIES = [
    Entry("test", "/", "directory"),
    Entry("how-containers-work.pdf", "/reading/tech", "file", 10533132),
    Entry("file.txt", "/test", "file", 20),
]


@pytest.mark.parametrize(
    "metadata,folder,entry",
    [
        (common.FOLDER_METADATA[0], "", ENTRIES[0]),
        (common.FILE_METADATA[0], "/test", ENTRIES[2]),
        (common.FILE_METADATA[1], "/reading/tech", ENTRIES[1]),
    ],
)
def test_to_dir_entry(
    metadata: df.FolderMetadata | df.FileMetadata, folder: str, entry: Entry
):
    assert to_dir_entry(metadata, folder) == entry


def test_list_folder(mock_dbx):
    def dirname(e: Entry):
        return e.directory

    grouped = itertools.groupby(sorted(ENTRIES, key=dirname), key=dirname)
    for directory, dir_entries in grouped:
        list_folder_result = list(list_folder(mock_dbx, directory))
        assert all(i in list_folder_result for i in dir_entries)


@pytest.mark.parametrize(
    "path,instance_type,path_display",
    [
        (Path("/test/"), df.FolderMetadata, "/test"),
        (Path("/test/file.txt"), df.FileMetadata, "/test/file.txt"),
        (Path("/non-existent"), type(None), None),
    ],
)
def test_get_db_path_metadata(
    mock_dbx, path: Path, instance_type: type, path_display: str | None
):
    resp = get_db_path_metadata(mock_dbx, path)
    assert isinstance(resp, instance_type)
    if path_display is not None:
        assert resp is not None
        assert resp.path_display == path_display
        assert resp.name == os.path.basename(path)


@pytest.mark.parametrize("path", [(CURRENT), (ROOT)])
def test_get_db_path_meta_exceptions(mock_dbx, path: Path):
    with pytest.raises(MetadataException):
        get_db_path_metadata(mock_dbx, path)


@pytest.mark.parametrize(
    "filename,folder,collides",
    [("file.txt", "/test", True), ("new_file.txt", "", False)],
)
def test_detect_collision(mock_dbx, filename: str, folder: Path, collides: bool):
    assert collides == detect_collision(mock_dbx, filename, folder)


@pytest.mark.parametrize("filename,folder", [("file", Path("/this/should/raise"))])
def test_detect_collision_exceptions(mock_dbx, filename: str, folder: Path):
    with pytest.raises(dropbox.exceptions.DropboxException):
        detect_collision(mock_dbx, filename, folder)


@pytest.mark.parametrize(
    "filename,folder,mode",
    [
        ("something_else", Path("/"), None),
        ("new_file.txt", Path("/test"), None),
        ("file.txt", Path("/test"), df.WriteMode.overwrite),
        ("file.txt", Path("/test"), df.WriteMode.update),
    ],
)
def test_copy_file(mock_dbx, filename: str, folder: Path, mode: df.WriteMode | None):
    local = io.BytesIO()
    local.name = os.path.join(folder, filename)
    monkey = pytest.MonkeyPatch()
    monkey.setattr(os.path, "getsize", lambda x: len(x))
    func = functools.partial(copy_file, mock_dbx, local, folder)
    if mode is not None:
        func.keywords["mode"] = mode
    func()
    want_path = os.path.join(folder, filename)
    want_mode = mode if mode is not None else df.WriteMode.add
    mock_dbx.files_upload.assert_called_with(b"", want_path, mode=want_mode)


@pytest.mark.parametrize(
    "filename,folder,mode",
    [("file.txt", Path("/test"), None), ("file.txt", Path("/test"), df.WriteMode.add)],
)
def test_copy_file_exceptions(
    mock_dbx, filename: str, folder: Path, mode: df.WriteMode | None
):
    local = io.BytesIO()
    local.name = filename
    with pytest.raises(CollisionException):
        func = functools.partial(copy_file, mock_dbx, local, folder)
        if mode is not None:
            func.keywords["mode"] = mode
        func()

    local.name = "something_else"
    copy_file(mock_dbx, local, Path("/"))
    mock_dbx.files_upload.assert_called_with(
        b"", f"/{local.name}", mode=df.WriteMode.add
    )


def test_rearrange():
    tabulated_entries = {
        "name": ["test", "file.txt", "how-containers-work.pdf"],
        "directory": ["/", "/test", "/reading/tech"],
        "entry_type": ["directory", "file", "file"],
        "size": ["-", "20", "10.05M"],
    }
    assert rearrange_for_tabulate(ENTRIES) == tabulated_entries


@pytest.mark.parametrize(
    "path,path_type,path_exists",
    [
        (Path("/"), PathType.Local, True),
        (Path("/local/test/dir"), PathType.Local, True),
        (Path("relative_file.txt"), PathType.Local, False),
    ],
)
def test_explain_input_path_local(
    mock_dbx, path: Path, path_type: PathType, path_exists: bool
):
    runner = CliRunner()
    with runner.isolated_filesystem() as test_dir:
        if path_exists:
            path = Path(f"{test_dir}{path}")
            path.mkdir(parents=True, exist_ok=True)

        (got_path_type, got_path) = explain_input_path(mock_dbx, path)
        if not path.is_absolute():
            path = Path(os.path.join(test_dir, path))
        assert got_path == path
        assert got_path_type == path_type


@pytest.mark.parametrize(
    "path,metadata", list((Path(i.path_display), i) for i in common.DROPBOX_CONTENTS)
)
def test_explain_input_path_remote(
    mock_dbx, path: Path, metadata: df.FileMetadata | df.FolderMetadata
):
    (path_type, got) = explain_input_path(mock_dbx, path)
    # note: we're explicitly avoiding checking equality because
    # otherwise we'd have to set a whole bunch of other attributes on the
    # FileMetadata/FolderMetadata instances
    assert isinstance(got, type(metadata))
    assert got.path_display == metadata.path_display
    assert path_type == PathType.Dropbox
