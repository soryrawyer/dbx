# tests

this project uses [`pytest`](https://docs.pytest.org/en/7.2.x/) to run unit tests, and each module in [`dbx`](../dbx) should have at most one corresponding test module. pytest fixtures that could be reused across test modules should be added to [`common.py`](./common.py)

the `mock_dbx` fixture is a mocked dropbox client and should be used whenever testing a function that relies on the dropbox api. mocked functions are used when the return value of the dropbox client is important to the rest of the code (an example where this _isn't_ the case would be, say, `copy_file`, where we just care about whether the call raised an exception). to create a new mocked function:

1. ensure you have a test dropbox account with a file structure that matches the test fixtures
2. call the real dropbox api with a few test cases
3. use the responses as part of the mocked function
4. follow the existing pattern to set the dropbox function on the mocked client to use the new mocked function
