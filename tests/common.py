"""
reusable test data and a mocked dropbox client object
"""

import io
import itertools
import os
from pathlib import Path
from typing import IO

from unittest.mock import MagicMock

import dropbox
import dropbox.exceptions
import dropbox.files as df
from requests.models import Response

import pytest


def get_meta_by_path(
    path: str, dropbox_contents: list[df.FileMetadata | df.FolderMetadata]
) -> df.FileMetadata | df.FolderMetadata | None:
    for entry in dropbox_contents:
        if path == entry.path_display:
            return entry
    return None


# TODO: check out snapshot tests
# this kind of state-checking feels like maybe a good use case
# just, instead of generating html we'd be generating new pytest fixtures
FOLDER_METADATA = [
    df.FolderMetadata(name="test", path_display="/test"),
    df.FolderMetadata(name="reading", path_display="/reading"),
    df.FolderMetadata(name="tech", path_display="/reading/tech"),
]


FILE_METADATA = [
    df.FileMetadata(name="file.txt", path_display="/test/file.txt", size=20),
    df.FileMetadata(
        name="how-containers-work.pdf",
        path_display="/reading/tech/how-containers-work.pdf",
        size=10533132,
    ),
]


DROPBOX_CONTENTS = FOLDER_METADATA + FILE_METADATA


LOOKUP_NOT_FOUND = df.LookupError("not_found", None)


def create_api_error(error):
    return dropbox.exceptions.ApiError(
        request_id="",
        error=error,
        user_message_locale=None,
        user_message_text=None,
    )


def files_list_folder_mock():
    def func(folder: str, *args, **kwargs):
        del args, kwargs
        if folder == "/":
            raise dropbox.exceptions.BadInputError(
                request_id="",
                message="can't list the root directory as '/', must pass empty string",
            )
        elif folder == "":
            # set the root directory back to "/" because that matches the return value of
            # something like os.path.dirname("/directory")
            folder = "/"
        elif folder[-1] == "/":
            folder = folder[:-1]

        def dirname(e: df.FolderMetadata | df.FileMetadata):
            return os.path.dirname(e.path_display)

        for remote_folder, dir_entries in itertools.groupby(
            sorted(DROPBOX_CONTENTS, key=dirname), key=dirname
        ):
            if remote_folder == folder:
                return df.ListFolderResult(entries=list(dir_entries), has_more=False)
        else:
            raise create_api_error(df.ListFolderError("path", LOOKUP_NOT_FOUND))

    return func


def files_get_metadata_mock():
    def func(path: str):
        if path == "/" or path == "":
            raise dropbox.exceptions.BadInputError(
                request_id="",
                message="can't get metadata for the root directory :/",
            )

        entry = get_meta_by_path(path, DROPBOX_CONTENTS)
        if entry is not None:
            return entry
        else:
            raise create_api_error(df.GetMetadataError("path", LOOKUP_NOT_FOUND))

    return func


def init_dropbox_file_contents() -> dict[str, IO]:
    result = dict()
    for file_meta in FILE_METADATA:
        result[file_meta.path_display] = io.BytesIO(b"hello")
    return result


def files_download_mock():
    contents = init_dropbox_file_contents()

    def func(path: Path):
        for element in DROPBOX_CONTENTS:
            if element.path_display != str(path):
                continue
            resp = Response()
            resp.raw = contents[element.path_display]
            return (element, resp)
        raise Exception(f"{str(path)} not found")

    return func


def files_upload_mock():
    def inner(content, path, mode):
        """
        raise an exception if path already exists and mode is WriteMode.add
        """
        del content  # we'll need this later, just.. not now
        entry = get_meta_by_path(path, DROPBOX_CONTENTS)
        if entry is None or mode != df.WriteMode.add:
            # return without error if either the target location doesn't exist
            # or we're planning to overwrite it anyway
            # TODO: another possible non-failure mode is if the content matches
            return
        else:
            raise create_api_error(
                df.UploadError(
                    "path",
                    df.UploadWriteFailed(
                        reason=df.WriteError(
                            "conflict", df.WriteConflictError("file", None)
                        )
                    ),
                )
            )

    return inner


@pytest.fixture()
def mock_dbx():
    mock = MagicMock(spec=dropbox.Dropbox)
    mock.files_list_folder = MagicMock(side_effect=files_list_folder_mock())
    mock.files_get_metadata = MagicMock(side_effect=files_get_metadata_mock())
    mock.files_download = MagicMock(side_effect=files_download_mock())
    mock.files_upload = MagicMock(side_effect=files_upload_mock())
    return mock
