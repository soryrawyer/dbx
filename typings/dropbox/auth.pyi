from _typeshed import Incomplete
from stone.backends.python_rsrc import stone_base as bb

class AccessError(bb.Union):
    other: Incomplete
    @classmethod
    def invalid_account_type(cls, val): ...
    @classmethod
    def paper_access_denied(cls, val): ...
    def is_invalid_account_type(self): ...
    def is_paper_access_denied(self): ...
    def is_other(self): ...
    def get_invalid_account_type(self): ...
    def get_paper_access_denied(self): ...

AccessError_validator: Incomplete

class AuthError(bb.Union):
    invalid_access_token: Incomplete
    invalid_select_user: Incomplete
    invalid_select_admin: Incomplete
    user_suspended: Incomplete
    expired_access_token: Incomplete
    route_access_denied: Incomplete
    other: Incomplete
    @classmethod
    def missing_scope(cls, val): ...
    def is_invalid_access_token(self): ...
    def is_invalid_select_user(self): ...
    def is_invalid_select_admin(self): ...
    def is_user_suspended(self): ...
    def is_expired_access_token(self): ...
    def is_missing_scope(self): ...
    def is_route_access_denied(self): ...
    def is_other(self): ...
    def get_missing_scope(self): ...

AuthError_validator: Incomplete

class InvalidAccountTypeError(bb.Union):
    endpoint: Incomplete
    feature: Incomplete
    other: Incomplete
    def is_endpoint(self): ...
    def is_feature(self): ...
    def is_other(self): ...

InvalidAccountTypeError_validator: Incomplete

class PaperAccessError(bb.Union):
    paper_disabled: Incomplete
    not_paper_user: Incomplete
    other: Incomplete
    def is_paper_disabled(self): ...
    def is_not_paper_user(self): ...
    def is_other(self): ...

PaperAccessError_validator: Incomplete

class RateLimitError(bb.Struct):
    reason: Incomplete
    retry_after: Incomplete
    def __init__(self, reason: Incomplete | None = ..., retry_after: Incomplete | None = ...) -> None: ...

RateLimitError_validator: Incomplete

class RateLimitReason(bb.Union):
    too_many_requests: Incomplete
    too_many_write_operations: Incomplete
    other: Incomplete
    def is_too_many_requests(self): ...
    def is_too_many_write_operations(self): ...
    def is_other(self): ...

RateLimitReason_validator: Incomplete

class TokenFromOAuth1Arg(bb.Struct):
    oauth1_token: Incomplete
    oauth1_token_secret: Incomplete
    def __init__(self, oauth1_token: Incomplete | None = ..., oauth1_token_secret: Incomplete | None = ...) -> None: ...

TokenFromOAuth1Arg_validator: Incomplete

class TokenFromOAuth1Error(bb.Union):
    invalid_oauth1_token_info: Incomplete
    app_id_mismatch: Incomplete
    other: Incomplete
    def is_invalid_oauth1_token_info(self): ...
    def is_app_id_mismatch(self): ...
    def is_other(self): ...

TokenFromOAuth1Error_validator: Incomplete

class TokenFromOAuth1Result(bb.Struct):
    oauth2_token: Incomplete
    def __init__(self, oauth2_token: Incomplete | None = ...) -> None: ...

TokenFromOAuth1Result_validator: Incomplete

class TokenScopeError(bb.Struct):
    required_scope: Incomplete
    def __init__(self, required_scope: Incomplete | None = ...) -> None: ...

TokenScopeError_validator: Incomplete
token_from_oauth1: Incomplete
token_revoke: Incomplete
ROUTES: Incomplete
